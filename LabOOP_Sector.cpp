/*
 * main.cpp
 *
 *  Created on: 7 нояб. 2020 г.
 *      Author: user
 */

#include <iostream>
#include <cmath>

using namespace std;

//#define DEBUG

class Figure
{
private:
	int c; // цвет
	bool visible; // свойство видимости
protected:
	int x, y; // базовая точка
	virtual void draw() const = 0; // отрисовать фигуру
public:
	Figure(int color, int x, int y)
	{
		this->x = x;
		this->y = y;
		this->c = color;
		visible = false;
#ifdef DEBUG
		cout << "Init Figure\n";
#endif
	}

	virtual ~Figure()
	{
#ifdef DEBUG
		cout << "Delete Figure\n";
#endif
	}
	void move(int dx, int dy) // сместить фигуру на (dx,dy) – только видимую
	{
		if (visible)
		{
			x += dx;
			y += dy;
#ifdef DEBUG
			cout << "Figure is shifted on (" << dx << ", " << dy << ").\n";
#endif
			draw();
		}
	}
	virtual void setBorderColor(int c) // установить цвет фигуры – только видимой
	{
		if (visible)
		{
			this->c = c;
#ifdef DEBUG
			cout << "Set border color: " << c << ".\n";
#endif
			draw();
		}
	}
	virtual int getBorderColor() const // получить цвет
	{
		return c;
	}
	void setVisible(bool isVisible = true) // показать/спрятать фигуру
	{
		visible = isVisible;

#ifdef DEBUG
		if (visible)
			cout << "Figure set visible.\n";
		else
			cout << "Figure set invisible.\n";
#endif
		if (visible)
			draw();
	}
	bool isVisible() const // признак видимости
	{
		return visible;
	}
	virtual void calcParams(float &perimeter, float &area) const = 0; // вычислить периметр и площадь фигуры
};

class Sector: public Figure
{
private:
	const float PI = 3.14;

protected:
	int angle1; // начальный угол
	int angle2; // конечный угол
	int radius; // радиус сектора
	virtual void draw() const
	{
		printf(
				"Draw Sector {borderColor:%d; centerX:%d; centerY:%d; radius:%d; angle1:%d; angle2:%d}\n",
				getBorderColor(), x, y, radius, angle1, angle2);
	}
public:
	Sector(int color, int centerX, int centerY, int radius, int angle1,
			int angle2) :
			Figure(color, centerX, centerY)
	{
		this->angle1 = angle1;
		this->angle2 = angle2;
		this->radius = radius;

#ifdef DEBUG
		cout << "Init Sector\n";
#endif
	}
	virtual ~Sector()
	{
#ifdef DEBUG
		cout << "Delete Sector\n";
#endif
	}
	virtual void calcParams(float &perimeter, float &area) const
	{
		area = PI * radius * radius * abs(angle1 - angle2) / 360;
		perimeter = PI * radius * abs(angle1 - angle2) / 180 + 2 * radius;
	}
	virtual void setSizes(int radius, int angle1, int angle2) // установить новые размеры
	{
		this->radius = radius;
		this->angle1 = angle1;
		this->angle2 = angle2;
#ifdef DEBUG
		cout << "Sector got new sizes.\n";
#endif

		if (isVisible())
			draw();
	}
};

class FilledSector: public Sector
{
protected:
	int fillColor; // цвет заполнения фигуры
	virtual void draw() const
	{
		printf(
				"Draw FilledSector {borderColor:%d; fillColor:%d; centerX:%d; centerY:%d; radius:%d; angle1:%d; angle2:%d}\n",
				getBorderColor(), fillColor, x, y, radius, angle1, angle2);
	}
public:
	FilledSector(int borderColor, int fillColor, int centerX, int centerY,
			int radius, int angle1, int angle2) :
			Sector(borderColor, centerX, centerY, radius, angle1, angle2)
	{
		this->fillColor = fillColor;

#ifdef DEBUG
		cout << "Init FilledSector\n";
#endif
	}

	virtual ~FilledSector()
	{
#ifdef DEBUG
		cout << "Delete FilledSector\n";
#endif
	}

	virtual void setFillColor(int color) // установить цвет заполнения фигуры
	{
		this->fillColor = color;

#ifdef DEBUG
		cout << "Set fill color: " << color << "\n";
#endif

		if (isVisible())
			draw();
	}
	virtual int getFillColor() const // получить цвет заполнения фигуры
	{
		return fillColor;
	}
	virtual void setBorderColor(int color) // перегрузка - установить цвет границы фигуры
	{
		if (fillColor != color)
			Figure::setBorderColor(color);
	}
};

void testFigure(Figure *f) // nc
{
	cout << "Testing..." << endl;

	float perim, area;
	f->calcParams(perim, area);
	cout << "perimeter: " << perim << ", area: " << area << endl;
	f->move(4, 3);
	f->setVisible(false);
	f->setVisible(true);
	cout << "visible: " << f->isVisible() << endl;
	cout << "border color: " << f->getBorderColor() << endl;
	f->setBorderColor(f->getBorderColor() + 2);
	cout << "new border color: " << f->getBorderColor() << endl;

	cout << "############" << endl;
}

int main()
{
#ifdef DEBUG
	cout << "Debugging\n";
#endif

	//Figure - Sector
	Figure *f1 = new Sector(1, 0, 0, 5, 10, 70);
	f1->setVisible(true);
	testFigure(f1);
	((Sector*) f1)->setSizes(50, 0, 180);
	testFigure(f1);
	delete f1;
	cout << endl << "************" << endl;

	//Figure - FilledSector
	FilledSector *fSector = new FilledSector(4, 1, 0, 0, 5, 10, 70);
	fSector->setVisible(true);
	testFigure(fSector);
	fSector->setFillColor(4);
	fSector->setBorderColor(5);
	fSector->setBorderColor(5);
	testFigure(fSector);
	delete fSector;

	return 0;

}

